# frozen_string_literal: true

require 'aws-sdk'
require 'yaml'
require 'stringex'
require 'time'
require 'middleman'

def to_source_path(path)
  path = path.sub(%r{^/}, '')
  if %r{/$} =~ path
    "#{path}index.html"
  else
    path
  end
end

desc 'Update S3 redirects.'
task :update_s3_redirects do
  raise ArgumentError, 'AWS_ACCESS_KEY_ID variable is empy' if ENV['AWS_ACCESS_KEY_ID'].nil?
  raise ArgumentError, 'AWS_SECRET_ACCESS_KEY variable is empy' if ENV['AWS_SECRET_ACCESS_KEY'].nil?
  raise ArgumentError, 'BUCKET_NAME variable is empty' if ENV['BUCKET_NAME'].nil?

  region = 'ap-northeast-1'

  s3 = Aws::S3::Resource.new(region: region)
  bucket = s3.bucket(ENV['BUCKET_NAME'])

  path = 'data/redirects.yml'
  data = YAML.load_file(path)
  raise ArgumentError, 'invalid or empty YAML file' unless data.is_a?(Array)

  data.map do |entry|
    sources = Array(entry['sources'])
    target = entry['target']

    sources.map do |source|
      object = bucket.object(to_source_path(source))
      object.put(website_redirect_location: target)
      puts "Redirect '#{object.key}' to '#{target}'"
    end
  end
  puts 'Update S3 redirects.'
end

desc "Fix links in the features page."
task :fix_links_in_features_page do
  text = File.read("./data/features.yml")
  text.gsub!(/^(\s+link:) (\/.+)$/, '\1 https://about.gitlab.com\2')
  File.write("./data/features.yml", text)
  puts "Fix links in the features page."
end

desc 'Begin a new post'
task :new_post, :title do |t, args|
  if args.title
    title = args.title
  else
    puts '記事のタイトルを英語で入力してください: '
    title = STDIN.gets.chomp
  end

  filename = "source/blog/blog-posts/#{Time.now.strftime('%Y/%m/%d')}/#{title.to_url}/index.html.md.erb"
  puts "新しい記事を作成します: #{filename}"
  FileUtils.mkdir_p(File.dirname(filename))
  File.open(filename, 'w') do |post|
    post.puts '---'
    post.puts "title: \"#{title.gsub(/&/, '&amp;')}\""
    post.puts 'author: Firstname Lastname # if name includes special characters use double quotes "First Last"'
    post.puts 'author_gitlab: GitLab.com username # ex: johndoe'
    post.puts 'author_twitter: Twitter username or gitlab # ex: johndoe'
    post.puts 'image_title: "/images/blogimages/post-cover-image.jpg"'
    post.puts 'description: "Short description for the blog post"'
    post.puts 'tags: tag1, tag2, tag3'
    post.puts 'ee_cta: false # required only if you do not want to display the EE-trial banner'
    post.puts "twitter_text: \"Text to tweet\" # optional;  If no text is provided it will use post's title."
    post.puts '---'
    post.puts ''
    post.puts '<!-- ここに記事のサマリーを書いてください -->'
    post.puts '<!-- moreより前の文章がサマリーとしてRSSフィードに出力されます -->'
    post.puts ''
    post.puts '<!-- more -->'
    post.puts ''
    post.puts '* コンテンツ'
    post.puts '{:toc}'
    post.puts ''
    post.puts '<!-- ここに記事のコンテンツを書いてください -->'
  end
end

desc 'Migrate blog artcle paths'
task :migrate_blog_article_paths do
  Dir.glob("source/blog/blog-posts/*").each do |path|
    # /source/blog/blog-posts/{yyyy}-{mm}-{dd}-{title}-released.html.md を
    # /source/releases/posts/{yyyy}-{mm}-{dd}-{title}-released.html.md に移動する
    regex_release_path = /(\d{4})-(\d{2})-(\d{2})-(.+-released\.html\.md)/
    mathed = regex_release_path.match(path)
    if mathed
      new_path = "source/releases/posts/#{mathed[1]}-#{mathed[2]}-#{mathed[3]}-#{mathed[4]}"
      puts "Migrate #{path} to #{new_path}"
      FileUtils.mkdir_p(File.dirname(new_path))
      FileUtils.mv(path, new_path)
      next
    end

    # /source/blog/blog-posts/{yyyy}-{mm}-{dd}-{title}.{ext} を
    # /source/blog/blog-posts/{yyyy}/{mm}/{dd}/{title}/index.{ext} に移動する
    regex_blog_path = /(\d{4})-(\d{2})-(\d{2})-([^.]+)\.(.+)/
    mathed = regex_blog_path.match(path)
    if mathed
      new_path = "source/blog/blog-posts/#{mathed[1]}/#{mathed[2]}/#{mathed[3]}/#{mathed[4]}/index.#{mathed[5]}"
      puts "Migrate #{path} to #{new_path}"
      FileUtils.mkdir_p(File.dirname(new_path))
      FileUtils.mv(path, new_path)
    end
  end
end