#!/bin/env ruby

# Usage
# ruby script/prepare-release-post.rb 11.7 2019-01-22 <src dir> <dest dir>

require 'fileutils'

version = ARGV[0]
day = ARGV[1]
src_dir = ARGV[2]
dest_dir = ARGV[3]

post_file = "source/posts/#{day}-gitlab-#{version.sub('.', '-')}-released.html.md"
data_file = "data/release_posts/#{day.tr('-', '_')}_gitlab_#{version.sub('.', '_')}_released.yml"
image_dir = "source/images/#{version.sub('.', '_')}"
twitter_image = "source/images/tweets/gitlab-#{version.sub('.', '-')}-released.png"

[post_file, data_file, image_dir, twitter_image].each do |path|
  src_full_path = File.expand_path("#{src_dir}/#{path}")
  dest_full_path = File.expand_path("#{dest_dir}/#{path}")

  FileUtils.rm_r(dest_full_path)
  FileUtils.cp_r(src_full_path, dest_full_path, verbose: true)
end

puts '## ToDo'
puts
puts '作業を **開始したら** チェックを入れてください。'
puts

data_full_path = File.expand_path("#{dest_dir}/#{data_file}")

puts "### #{data_file}"
puts

regex = /^(\s+- )((?:feature_)?name: )(.+)/
lines = []
File.foreach(data_full_path) do |line|
  matched = line.match(regex)
  if matched
    puts "- [ ] #{matched[3].delete('"')}"
    lines << "#{matched[0]}\n"
    lines << "#{matched[1].sub('-', ' ')}#{matched[2].sub('name', 'name_en')}#{matched[3]}\n"
  else
    lines << line
  end
end

File.write(data_full_path, lines.join)

post_full_path = File.expand_path("#{dest_dir}/#{post_file}")

puts
puts "## #{post_file}"
puts

regex = /^## (.+)/
File.foreach(post_full_path) do |line|
  matched = line.match(regex)
  puts "- [ ] #{matched[1]}" if matched
end
