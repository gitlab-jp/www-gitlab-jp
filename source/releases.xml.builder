xml.instruct!
xml.feed "xmlns" => "http://www.w3.org/2005/Atom" do
  site_url = "https://www.gitlab.jp/"
  xml.title "GitLab日本語情報サイト - リリース情報"
  xml.id URI.join(site_url, blog("releases").options.prefix.to_s)
  xml.link "rel" => "alternate", "href" => site_url
  xml.updated(blog("releases").articles.first.mtime.to_datetime.rfc3339) unless blog("releases").articles.empty?

  blog("releases").articles[0...2].each do |article|
    xml.entry do
      xml.title article.title
      xml.link "rel" => "alternate", "href" => URI.join(site_url, article.url)
      xml.id URI.join(site_url, article.url)
      xml.published article.date.to_datetime.rfc3339
      xml.updated article.mtime.to_datetime.rfc3339
      xml.author { xml.name article.data.author }
      xml.summary article.summary, "type" => "html"
      # xml.content article.body, "type" => "html"
    end
  end
end
