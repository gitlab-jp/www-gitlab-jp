---
layout: markdown_page
title: "Category Direction - Usability Testing"
description: "Usability testing is a technique in user-centered interaction design to evaluate a product by testing it on users and is an irreplaceable usability practice"
canonical_path: "/direction/verify/usability_testing/"
---

- TOC
{:toc}

## ユーザビリティテスト

GitLab has lots of capability for testing various aspects of software, but most are aimed around functional testing of code. Usability testing is a technique used in user-centered interaction design to evaluate a product by testing it on users. This can be seen as an irreplaceable usability practice, since it gives direct input on how real users use the system.

Usability testing is slightly different than UAT in that the primary purpose of a usability test is to improve a design, rather than a gate to accept/reject a deployment. At GitLab, we're not big believers in manual gates to slow things down, but we are big believers in user-focused design.

One thing that we certainly want to communicate through our vision here is that we feel designers are important, and can contribute using GitLab. By adding features with them in mind, we can help them to contribute their best.

Interested in joining the conversation for this category? Please join us in the category's issues
where we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

- [Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/960)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AUsability%20Testing)
- [Overall Vision](/direction/ops/#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

The next thing we are working on for Usability Testing is [&3085](https://gitlab.com/groups/gitlab-org/-/epics/3085) which will bring the ability to take and attach screenshots to comments from the Visual Review tool. This will help users simplify their workflow and not have to take a screenshot and attach it to the comment after it is created in the MR. This also allows users without an account or an account without permissions to add those screenshots which they cannot do today.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

* [Add screenshots to Visual Review Tool](https://gitlab.com/groups/gitlab-org/-/epics/3085)
* [Add full-screen annotations to Visual Review tool](https://gitlab.com/gitlab-org/gitlab/issues/10762)

We may find in research that only some of these issues are needed to move the vision for this category forward. The work to move the vision is captured and being tracked in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/960).

## Competitive Landscape

Usability testing is a feature offered primarily through integrations by competitive products. Taking advantage of Review Apps, we're able to offer a much nicer experience via [gitlab-org&960](https://gitlab.com/groups/gitlab-org/-/epics/960) (manual review flow for review apps) where feedback can be requested on a review environment, bringing feedback much earlier into the development process and integrating that automatically with GitLab issues.

### Marker.io

[Marker.io](https://marker.io/) is a product that ships mostly as an extension to go into your browser and allows you to raise an issue with various issue providers (Trello, JIRA, GitHub and even GitLab).  It provides a very intuitive user interface for screenshots, annotations and other discussions.

## Top Customer Success/Sales Issue(s)

There are no top CS/Sales issues for this category at this time.

## Top Customer Issue(s)

The most popular issue for the category is [gitlab#27228](https://gitlab.com/gitlab-org/gitlab/-/issues/27228) which adds image annotations for the Visual Review tool.

## Top Internal Customer Issue(s)

For usability testing, our internal team has asked for built-in support for visual regression testing ([gitlab#26489](https://gitlab.com/gitlab-org/gitlab/issues/26489)).

## Top Vision Item(s)

When we think about the long term vision of the Usability category we see opportunities to build in support for [visual regression testing](https://gitlab.com/gitlab-org/gitlab/issues/26489) to help developers see visual degradations before they merge.

Beyond this, we can look at offering the same for mobile via a solution like https://headspin.io/products.
