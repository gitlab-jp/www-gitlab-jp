---
layout: markdown_page
title: "Category Direction - Authorization and Authentication"
description: "Authentication and authorization are critical, foundational elements to keeping resources secure but accessible. Learn more here!"
canonical_path: "/direction/manage/auth/"
---

- TOC
{:toc}

| Category | **Authorization and Authentication** |
| --- | --- |
| Stage | [Manage](https://about.gitlab.com/handbook/product/product-categories/#manage-stage) |
| Group | [Access](https://about.gitlab.com/handbook/product/product-categories/#access-group) |
| Maturity | [Complete](/direction/maturity/) |
| Content Last Reviewed | `2020-08-04` |

## Introduction and how you can help

Thanks for visiting the direction page for Authentication and Authorization in GitLab. This page is being actively maintained by [Melissa Ushakov](https://about.gitlab.com/company/team/#mushakov). This vision and direction is a work in progress and sharing your feedback directly on issues and epics on GitLab.com is the best way to contribute.

## 概要

Authentication and authorization are critical, foundational elements to keeping resources secure but accessible. This is of particular importance for large enterprises or companies operating in regulated environments; for an authentication strategy to function, it must be secure and scalable. Onboarding/offboarding new employees should be automatable and not allow unauthorized users to access sensitive information.

Furthermore, security strategies have evolved from one of assumed trust within a trusted network. Historically, most organizations assume that authenticated users coming from a trusted source are allowed to access resources - those outside, if not on a VPN, cannot. However, security perimeters have become increasingly porous and an assumed trust model begins to break down with more services shifting to cloud and more employees BYODing or working remotely.

While GitLab's supported strategies meet most expectations (especially outside of the enterprise), our strategy is to aggressively invest in further improvements to authentication and authorization, with a particular focus on SAML.

### Target audience and experience

The primary audience for future effort are administrators in medium to large enterprises. These are privileged, sophisticated users in companies managing employee identities with a single source of truth; this may be a series of LDAP servers or an IdAAS service like Okta. Automation matters - we should minimize the amount of manual work needed to onboard/offboard an employee and be able to assign permissions automatically - but the must-have is security, especially in sensitive environments operating under regulatory scrutiny. We need to strike a balance between being overly permissive (e.g. unintentionally allowing access to an offboarded employee) and overly restrictive (e.g. getting in the way of a developer's workflow and annoying them with reauthentication requests).

## Where we are Headed

We're currently focused on broader and deeper support for identity management and authentication strategies. Our objective is to allow users to quickly join GitLab with the right level of access, and building support for large organizations to quickly onboard and offboard users is essential.

For large organizations, we also need to make sure that users are armed with the right level of access. GitLab's role-based access control has served small teams well - and fits with a permissions model where anyone can contribute - but makes it challenging for large, security-minded organizations to build the level of granular controls and variation they need.

### What's Next and Why

On theme with the two areas of focus above - deeper support for identity management and fine-grained access controls - Access is  focused on these areas in FY21Q3:

* On authentication, Access is prioritizing SAML SSO maturity. We are specifically focusing on capabilities that make setup and user management at scale easier.
   * [IDP-specific support for SAML and SCIM](https://gitlab.com/groups/gitlab-org/-/epics/1900)
* On authorization, we're prioritizing improvements that provide much-needed flexibility to allow administrators to define their access levels, assign to groups and define hierarchies.
   * [SAML Group Sync](https://gitlab.com/gitlab-org/gitlab/-/issues/118)
   * [Policies ABAC MVC](https://gitlab.com/gitlab-org/gitlab/issues/7626)
* Improving quality, security and performance will also continue to an area of focus.

You can see more details on our roadmap in our [Maturity Plan](#maturity-plan) below.

## Maturity Plan

This category is currently **Complete**. The next step in our maturity plan is achieving a **Lovable** state.
* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.
* Note that a `Complete` state does not mean a category is "finished" and is no longer a priority. Even categories that are considered `Lovable` require continued investment.

While authentication and authorization in GitLab has a sufficient feature set to be competitive, we see significant opportunity to elevate our capabilities - especially for the enterprise. You can see the current scope of work to achieve this state in [the corresponding Lovable maturity epic](https://gitlab.com/groups/gitlab-org/-/epics/1964).
