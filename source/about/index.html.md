---
layout: markdown_page
title: このサイトについて
---

このサイトは、GitLabを日本で広めるために個人で開設したコミュニティサイトです。

なお、GitLab B.V.の公式サイトは
[about.gitlab.com](https://about.gitlab.com)
であり、GitLab B.V.はこのサイトの運営には携わっていないのでご注意ください。

このサイトを開設する際、事前にGitLab B.V.のCEOのSytse Sijbrandij氏にメールを送り、
[about.gitlab.com](https://about.gitlab.com) の [ソース](https://gitlab.com/gitlab-com/www-gitlab-com)
を翻訳して www.gitlab.jp として公開しても良いか確認をして許可をいただきました。

現在、このサイトの管理人はGitLabの
[国内の正規代理店](https://www.creationline.com/) に所属し、
[国内のユーザーグループ](https://gitlab-jp.connpass.com/) の管理者の一人を務めています。
