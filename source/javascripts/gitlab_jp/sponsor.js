(function() {
  var sponsorLogo = document.getElementById('sponsor-logo');
  if (sponsorLogo.width >= (sponsorLogo.height * 2)) {
    sponsorLogo.style.width = '420px';
  } else {
    sponsorLogo.style.width = '260px';
  }
})();
