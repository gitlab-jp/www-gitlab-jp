---
layout: markdown_page
title: "Google CloudBuild"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## このページのコンテンツ
{:.no_toc}

- TOC
{:toc}

## 要約
Cloud Build lets you build software quickly across all languages. Get complete control over defining custom workflows for building, testing, and deploying across multiple environments such as VMs, serverless, Kubernetes, or Firebase.

## リソース
* [Google CloudBuild](https://cloud.google.com/cloud-build/)

## Integrations
* Google CloudBuild and GitLab partnered to create a demo for Google Next 2018. This showed 2 use cases:
   - Using GitLab for SCM-only and CloudBuild for CI/CD
   - Using GitLab CI/CD test and deploy, but offloading the build stage to CloudBuild

   [Full details, as well as links to the demo and sample code](https://about.gitlab.com/blog/2018/07/27/google-next-2018-recap/#google-cloud-build--gitlab-cicd) are on the GitLab blog.

## 料金
- [Google Pricing Guide](https://cloud.google.com/cloud-build/pricing)
- First 120 build-minutes per day - Free
- Additional build-minutes - $0.0034 per minute
- Note: Google Cloud Platform costs apply on top of these costs
