---
layout: markdown_page
title: "Redmine"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Challenges
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## このページのコンテンツ
{:.no_toc}

- TOC
{:toc}

## 要約
Redmineは無料でオープンソースのWebベースのプロジェクト管理・課題追跡ツールです。複数のプロジェクトと関連するサブプロジェクトを管理することができます。プロジェクトごとのWikiやフォーラム、タイムトラッキング、柔軟性のあるロールベースのアクセス制御などが特徴です。カレンダーとガントチャートが含まれており、プロジェクトとその期限を視覚的に表現するのに役立ちます。Redmineは様々なバージョン管理システムと統合されており、リポジトリブラウザとdiffビューアが含まれています。

RedmineはRuby on Railsで書かれています。クロスプラットフォームとクロスデータベースで、UIは34の言語で翻訳されています。


## コメント/逸話
* [Quoraのユーザー](https://www.quora.com/What-is-your-experience-using-Redmine-for-project-management-and-collaborating)から
   > 柔軟性があり、カスタマイズ性が高く、（様々なテンプレートやプラグインなどから選ぶことができます）オープンソースであり、無料で利用できます。[そして]ユーザーインターフェースは分かりやすいです。
   >
   > カスタマイズ性が高いので、チームメンバーや同僚/クライアントに素晴らしいユーザー体験を提供するためには、まず自分自身で使いこなす必要があります。

## リソース
* [Redmineのホームページ](https://www.redmine.org/projects/redmine/wiki)
* [Redmine Wikipedia](https://en.wikipedia.org/wiki/Redmine)
* [G2クラウドのレビューページ](https://www.g2crowd.com/products/redmine/reviews)

## 料金
* ¥0 - Redmineはフリーでオープンソースです。
* しかし、RedmineはJenkinsと同じ用に、プラグイン（2018-10-17時点で944）を介して利用できる便利な機能がたくさんあり、その中にはメンテナンスされているものもあれば、そうでないものもあるため、運用コストは高くなる可能性があります。
* また、例えばアジャイル機能を追加するためのプラグインセットを販売している会社もあります（例、[RedmineUP Agile Plugin](https://www.redmineup.com/pages/plugins/agile))。価格は様々です。

## 比較
