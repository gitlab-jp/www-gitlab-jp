---
layout: markdown_page
title: "GitLab vs VictorOps"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## このページのコンテンツ
{:.no_toc}

- TOC
{:toc}

## 要約
VictorOps is incident management software that pulls in data from other sources like log management, monitoring, and chat tools to provide a single, unified view into system health. By automating alert delivery, VictorOps aims to provide a streamlined on-call experience to alert the correct people when they are needed, and provide them the data needed to resolve incidents quickly. In 2018, VictorOps was [acquired by Splunk](https://www.splunk.com/blog/2018/06/25/splunk-and-victorops-two-great-companies-working-towards-one-devops-vision.html). VictorOps itself does not provide functionality such as monitoring, logging, metrics, tracing, SCM, or issue management. Instead, it relies on a [variety of integrations](https://victorops.com/integrations/#) with other tools in order to provide incident managment.

GitLab provides an entire DevOps toolchain in a single application. From issue tracking and source code management to CI/CD, security, and monitoring, GitLab's approach provides all the data needed to pre-integrate it into a single holistic application. Today, GitLab provides a breadth of monitoring  features (like metrics, tracing, and logging) built in with some incident management capabilities. GitLab's [vision and roadmap](https://about.gitlab.com/direction/monitor/) for monitoring and incident management is to provide a depth of functionality in these categories. Because VictorOps supports sources that GitLab uses - such as Sentry for error tracking and Prometheus for time-series monitoring and alerts - it is possible to use GitLab together with VictorOps.


## リソース
* [VictorOps](https://victorops.com/)