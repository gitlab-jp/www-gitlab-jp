---
layout: markdown_page
title: "Buildkite"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## このページのコンテンツ
{:.no_toc}

- TOC
{:toc}


## 要約
Buildkite is a continuous integration tool designed to improve software developer productivity.  Buildkite product consists of three main components:
- Agent: Is a small, cross-platform build runner that is enables an enterprise to run its builds in their own infrastructure.  Agents run on several platforms including Ubuntu, Debian, Mac, Windows, Docker, and others.  Agents can only run one build job at one time.
- Pipelines: Are containers to define and model the build workflows and also to kick off post build steps.
- Deployments: These are steps that run after a build is complete and can be either triggered automatically or manually.

As a smaller company with limited resources, Buildkite has taken a strategy of using plug-ins to extend its functionality and to integrate with other software.  It is unclear how many community plug-ins have been developed.  However as on 3/10/2020 there are about 19 supported plug-ins provided by Buildkite.

## Strengths
- Parallel Testing: One of the key value props for Buildkite is the ability to run parallel tests on builds, thus reducing test times from hours to minutes.  Buildkite does this using its parallelism approach to spawn out multiple agents (e.g. several hundred parallel agents). With different tests running on a different agents.  This massive parallel testing is possible due to the light weight build agent.
- Runs on-premises and on AWS infrastructure
- Agents can be deployed on multiple platforms.
- Pipelines as code:  Ability to define complex pipelines in yml including running any scripts, tool or shell commands within the pipeline.

## 不足部分
- Buildkite is a point solution for continuous integration and currently lacks other critical DevOps steps such as Issue Management, Source Code Management, Security Testing etc.  These capabilities require integrations with other tools.
- Security - Buildkite relies on third party providers for security testing.  Calling third party code to launch security tests would require special scripts and execution of APIs from within Buildkite's pipeline definition.
- Lacks native support for an Artifact Repository.  File paths to various artifacts have to be noted and maintained by users.
- Secrets management is possible but users have to either write plug-ins to standard secrets management products such as Hashicorp or write custom scripts to pull secrets from a secure storage and apply them at the right step in the build process.
- Collaboration features are limited to developers annotating comments inline within log files and other points of failure.

## リソース
[Buildkite Documentation](https://buildkite.com/docs/tutorials/getting-started)

[Buildkite Videos](https://buildkite.com/screencasts)

## 比較
