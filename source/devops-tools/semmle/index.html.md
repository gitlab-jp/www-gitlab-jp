---
layout: markdown_page
title: "Semmle"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## このページのコンテンツ
{:.no_toc}

- TOC
{:toc}

## 要約
Semmle's code analysis platform helps teams find zero-days and automate variant analysis. Secure your code with continuous security analysis and automated code review.  The feature by feature comparison is below this table.

| Additional Context | Semmle | GitLab |
| -------------    | ------------- | ------------ |
| Ease of Use | Has Variant Analysis, but first you have to manually define the problem before you can code a variant for future use. | Uses multiple pre-defined industry leading standards to identify vulnerabilities. |
| Breadth of Application | Highly customized standards that are specific to the enterprise/application. | Broad visibility into common security vulnerabilities |
| Coding Standards | Enforce compliance with internal coding standards | Enforce compliance with defined coding standards |
| Expertise Needed | For Variant Analysis, need specific knowledge of QL syntax to create any variant. Heavy use of custom regex mapped to internal coding standards | Uses multiple pre-defined industry leading standards to identify vulnerabilities |
| Pattern maintenance | One might need to further customize patterns for each application/scenario because the standards/taxonomy likely keeps changing from app to app and release to release | Not applicable. No custom pattern definiton |
| Dependency Analysis | Custom queries to ID dependencies | Basic analysis is automated |
| Dead Code Analysis | Basic dead code analysis has to be extended with domain specific knowledge | Not currently supported |

On September 18, 2019 [GitHub acquired Semmle](https://techcrunch.com/2019/09/18/github-acquires-code-analysis-tool-semmle/).

## コメント/逸話

## リソース
* [Semmle website](https://semmle.com/)
* [Wikipedia page](https://en.wikipedia.org/wiki/Semmle)

## 比較
