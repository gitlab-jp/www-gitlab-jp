---
release_number: "11.10"
title: "GitLab 11.10がリリースされました。オペレーションダッシュボード、マージの結果に対するパイプライン、マージリクエストで複数行の変更を提案"
author: Noriko Akiyama
author_gitlab: n-akiyama
image_title: '/images/11_10/11_10-cover-image.jpg'
description: "GitLab 11.10 released with Pipelines on the Operations Dashboard, Pipelines for Merged Results, Multi-line Merge Request Suggestions, and much more!"
description: "GitLab 11.10がリリースされました。オペレーションダッシュボード、マージの結果に対するパイプライン、マージリクエストで複数行の変更を提案などたくさんの機能が追加！"
twitter_image: '/images/tweets/gitlab-11-10-released.png'
categories: releases
layout: release
featured: yes
release_number_dark: true
---

### プロジェクト間のパイプラインの状態を簡単に確認

GitLab は DevOps ライフサイクルを可視化するための機能を追加し続けています。
今回のリリースでは、パイプラインのステータスの概要を提供する強力な機能により
 [オペレーションダッシュボード](https://docs.gitlab.com/ee/user/operations_dashboard/) が拡張されています。

これは単一プロジェクトのパイプラインを見る場合でも便利ですが、
[マルチプロジェクトパイプライン](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html) を使用している場合は特に便利です。
アプリケーションをマイクロサービスアーキテクチャで作成する場合は、ソースコードはサービス毎に異なるリポジトリへ格納し、それぞれのリポジトリでテストやデプロイのパイプラインを実行するのが一般的です。
[オペレーションダッシュボード](#pipelines-on-the-operations-dashboard) を使用することで、関心のあるプロジェクトのパイプラインの状態をまとめて表示することができます。

### マージ後の結果に対するパイプラインの実行

ソースブランチとターゲットブランチが分岐してからの時間の経過とともに、それぞれのブランチの差分は大きくなっていきます。
この場合、ソースとターゲットの両方のパイプラインをパスしたとしても、
マージ後のパイプラインが失敗するというシナリオが発生する可能性があります。
GitLab 11.10では、マージを実施する前に [マージ後の結果に対してパイプラインを実行](#pipelines-for-merged-results) できます。
これにより、頻繁にリベースしなければ見つけられないエラーを素早く見つけることができるので、
パイプラインの失敗を迅速に解決したり [GitLab ランナー](https://docs.gitlab.com/runner/) をより効率良く利用することが可能になります。

### コラボレーションをさらに合理化

GitLab 11.10 では、コラボレーションと開発者のワークフローをシンプルにするためにさらに多くの機能を提供します。
[以前のリリース](/2019/01/29/gitlab-11-6-released/#suggested-changes) では、
レビュー担当者がコメントのスレッドインターフェイス内から簡単にコミットできるマージリクエストコメントの 1 行の変更を提案できるというマージリクエストの提案機能を導入しました。
GitLab ユーザはその機能を歓迎しています。
現在は、削除したい既存の行を指定したり、複数行を追加したりといった [複数行の変更](#suggest-changes-to-multiple-lines) を提案することができます。
改善提案していただきありがとうございます。

### その他

上記以外にも今回のリリースでは多くの素晴らしい機能がリリースされました。
例えば、[スコープ付きラベル](#scoped-labels),
さらに徹底化された [コンテナレジストリのクリーンアップ](#more-thorough-container-registry-cleanup),
[構成可能な Auto DevOps](#composable-auto-devops),
[CI ランナーを分単位で追加購入](#purchase-add-on-ci-runner-minutes) 機能などがあります。
これらの新機能を把握するために是非この記事を読んでください。
