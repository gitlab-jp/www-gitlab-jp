### www-gitlab-jp

これは https://www.gitlab.jp/ のソースです。

GitLab日本語公式情報サイトは[GitLab公式ページ](https://about.gitlab.jp)の[過去のソース](https://gitlab.com/gitlab-com/www-gitlab-com/commits/972f68328c1a7593e65cce76bc9630e33d97b44d)をベースに作成しています。

### 開発環境

#### 必要条件

- Ruby 2.5.1
- Bundler
- [Middleman](https://middlemanapp.com/jp/)

#### インストール

macOSにはRubyとRubyGemsの両方がパッケージされていますが、Middlemanの依存ライブラリの一部はインストール時にコンパイルする必要があります。macOSではコンパイルにXcodeのCommand Line Toolsが必要です。Xcodeがインストールされている場合Terminalから実行してください:

```
$ xcode-select --install
```

続いて、以下のコマンドを実行して下さい。`<path-to-repository>`はリポジトリをcloneしたパスです。各自の環境に合わせて変更して下さい。

```
$ cd <path-to-repository>
$ bundle install
```

#### ローカルプレビュー

下記コマンドで Middleman のサーバーが起動します。

```
$ bundle exec middleman
```

Middleman のサーバーが起動したら、ブラウザで http://localhost:4567 にアクセスするとサイトのローカルプレビューを表示できます。

#### ビルド

下記コマンドで本番環境用のファイルをビルドできます。
ビルド結果は `public` ディレクトリに出力されます。

```
$ bundle exec middleman build
```

### Rakeタスク

#### ブログ記事のテンプレートファイルを作成する

```
bundle exec rake new_post
```

ブログタイトルの入力が要求されるので、ブログのタイトルを **英語** で入力します。
これは、入力したタイトルがブログを公開するURLに変換されて利用されるため、日本語ではうまくURLに変換できないためです。

例えばブログのタイトルに「Hello World!」を入力すると `source/blog/blog-posts/yyyy/mm/dd/hello-world/index.html.md.erb`
というブログ記事のテンプレートファイルが作成されます。
このブログ記事は `/blog/yyyy/mm/dd/hello-world/` というURLで公開されます。
公開日やURLを変更したい場合はフォルダやファイルをリネームしてください。

できる限りブログにはカバーイメージを付けてください。
[Unsplash](https://unsplash.com/) で素敵な画像をたくさん見つけることができます。

マージリクエストの説明に使用するカバーイメージへのリンクをHTMLコメントで、次のように記載してください。

```
<!-- image: image-url -->
```

また、カバーイメージとして Unsplash の画像を利用した場合は、ブログの最後に必ず下記のようなクレジットを書いてください。

```
カバーイメージの提供: [画像の著者名](著者へのリンク) on [Unsplash](画像へのリンク)
{: .note}
```

#### S3のウェブページリダイレクトを設定する

`www-gitlab-jp`バケットの書き込みができるIAMアカウントの認証情報をセットする。

```
export AWS_ACCESS_KEY_ID={access key id}
export AWS_SECRET_ACCESS_KEY={secret access key}
```

S3のウェブページリダイレクトを設定する。

```
bundle exec rake update_s3_redirects
```
