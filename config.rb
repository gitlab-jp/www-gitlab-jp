require 'generators/direction'
require 'extensions/breadcrumbs'
require 'extensions/partial_build'
require 'lib/homepage'
require 'lib/mermaid'
require "thwait"

require 'lib/gitlab_jp/redirect'

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

activate :autoprefixer do |prefix|
  prefix.browsers = 'last 2 versions'
end

activate :syntax, line_numbers: false

set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

activate :blog do |blog|
  blog.name = 'blog'
  # This will add a prefix to all links, template references and source paths
  blog.prefix = 'blog'
  blog.sources = "blog-posts/{year}/{month}/{day}/{title}/index.html"
  blog.permalink = '/{year}/{month}/{day}/{title}/index.html'
  blog.layout = 'post'
  # Allow draft posts to appear on all branches except master (for Review Apps)
  blog.publish_future_dated = true if ENV['CI_COMMIT_REF_NAME'].to_s != 'master'

  blog.summary_separator = /<!--\s*more\s*-->/

  # blog.custom_collections = {
  #   categories: {
  #     link: '/blog/categories/{categories}/index.html',
  #     template: '/category.html'
  #   }
  # }
  # blog.tag_template = '/templates/tag.html'
  # blog.taglink = '/blog/tags/{tag}/index.html'
end

activate :blog do |blog|
  blog.name = 'releases'
  # This will add a prefix to all links, template references and source paths
  blog.prefix = 'releases'
  blog.sources = 'posts/{year}-{month}-{day}-{title}.html'
  blog.permalink = '/{year}/{month}/{day}/{title}/index.html'
  blog.layout = 'post'
  # Allow draft posts to appear on all branches except master (for Review Apps)
  blog.publish_future_dated = true if ENV['CI_BUILD_REF_NAME'].to_s != 'master'

  blog.summary_separator = /<!--\s*more\s*-->/

  # blog.custom_collections = {
  #   categories: {
  #     link: '/categories/{categories}/index.html',
  #     template: '/category.html'
  #   }
  # }
  # blog.tag_template = '/templates/tag.html'
  # blog.taglink = '/blog/tags/{tag}/index.html'
end

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# Proxy Comparison html and PDF pages
data.features.devops_tools.each_key do |devops_tool|
  next if devops_tool[0..6] == 'gitlab_'

  file_name = "#{devops_tool}-vs-gitlab".tr('_', '-')
  proxy "/devops-tools/#{file_name}.html", "/templates/comparison.html", locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }

  proxy "/devops-tools/pdfs/#{file_name}.html", '/devops-tools/pdfs/template.html', locals: {
    key_one: devops_tool,
    key_two: 'gitlab_ultimate'
  }, ignore: true
end

# Category pages for /stages-devops-lifecycle
data.categories.each do |key, category|
  next unless category.body && category.maturity && (category.maturity != "planned") && category.marketing

  proxy "/stages-devops-lifecycle/#{key.dup.tr('_', '-').downcase}/index.html", '/stages-devops-lifecycle/template.html', locals: {
    category: category,
    category_key: key
  }, ignore: true
end

# Sponsor page
data.sponsors.each do |sponsor|
  proxy "/sponsors/#{sponsor.name.mb_chars.normalize(:kd).gsub(/[^\x00-\x7F]/n, '').downcase.to_s.tr(' ', '-')}/index.html", '/sponsors/template.html', locals: {
  sponsor: sponsor
  }, ignore: true
end

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

activate :breadcrumbs, wrapper: :li, separator: '', hide_home: true, convert_last: false
activate :partial_build

configure :build do
  set :build_dir, 'public'
  set :base_url, '/www-gitlab-jp' # baseurl for GitLab Pages (project name) - leave empty if you're building a user/group website
  activate :minify_css
  activate :minify_javascript
  activate :minify_html

  # Populate the direction and releases pages only if INCLUDE_GENERATORS is true
  # That will help shave off some time of the build times when they are not needed
  if ENV['INCLUDE_GENERATORS'] == 'true'

    # Direction page
    if ENV['PRIVATE_TOKEN']
      content = {}
      direction = Generators::Direction.new

      wishlist = direction.generate_wishlist # wishlist, shared by most pages
      direction_all_content = direction.generate_direction(Generators::Direction::STAGES) # /direction/*
      direction_dev_content = direction.generate_direction(Generators::Direction::DEV_STAGES) # /direction/dev/
      direction_ops_content = direction.generate_direction(Generators::Direction::OPS_STAGES) # /direction/ops/
      direction_enablement_content = direction.generate_direction(Generators::Direction::ENABLEMENT_STAGES) # /direction/ops/

      Generators::Direction::STAGES.each do |name|
        # Fetch content for per-team pages
        skip = %w[release verify package] # these do not have a team page
        content[name] = direction.generate_direction(%W[#{name}]) unless skip.include? name # /direction/name/
      end

      stage_contribution_content = direction.generate_contribution_count(data.stages) # /direction/maturity/
      stage_velocity = direction.generate_stage_velocity(data.stages) # /direction/maturity/

      milestones = direction.generate_milestones # /releases/gitlab-com

      # Set up proxies using now-fetched content for shared pages
      proxy '/upcoming-releases/index.html', '/upcoming-releases/template.html', locals: { direction: direction_all_content }, ignore: true
      proxy '/direction/paid_tiers/index.html', '/direction/paid_tiers/template.html', locals: { wishlist: wishlist }, ignore: true
      proxy '/releases/gitlab-com/index.html', '/releases/gitlab-com/template.html', locals: { milestones: milestones }, ignore: true
      proxy '/direction/dev/index.html', '/direction/dev/template.html', locals: { direction: direction_dev_content }, ignore: true
      proxy '/direction/ops/index.html', '/direction/ops/template.html', locals: { direction: direction_ops_content }, ignore: true
      proxy '/direction/enablement/index.html', '/direction/enablement/template.html', locals: { direction: direction_enablement_content }, ignore: true
      proxy '/direction/maturity/index.html', '/direction/maturity/template.html', locals: { stage_contributions: stage_contribution_content, stage_velocity: stage_velocity }, ignore: true
      proxy '/direction/kickoff/index.html', '/direction/kickoff/template.html', locals: { direction: direction_all_content }, ignore: true
      proxy '/direction/moonshots/index.html', '/direction/moonshots/template.html', locals: { wishlist: wishlist }, ignore: true

      Generators::Direction::STAGES.each do |name|
        # And for team pages
        skip = %w[release verify package] # these do not have a team page
        proxy "/direction/#{name}/index.html", "/direction/#{name}/template.html", locals: { direction: content[name], wishlist: wishlist }, ignore: true unless skip.include? name
      end
    end
  end
end

configure :development do
  activate :livereload
end

# Don't render or include the following into the sitemap
ignore '/direction/*' unless ENV['PRIVATE_TOKEN']
ignore '/frontend/*'
ignore '/templates/*'
ignore '/includes/*'
ignore '/upcoming-releases/template.html'
ignore '/releases/template.html'
ignore '/releases/gitlab-com/template.html'
ignore '/company/team/structure/org-chart/template.html'
ignore '/source/stylesheets/highlight.css'
ignore 'source/job-families/check_job_families.py'
ignore '/category.html'
ignore '/.gitattributes'
ignore '**/.gitkeep'
ignore '/sites/*'
ignore '/jobs/apply/interim_landing_page.html.haml'
ignore '/jobs/apply/job-listing/template.html'

GitlabJp::Redirect.each_for_middleman do |source, target|
  redirect source, to: target
end

# ブログの公開日を日本時間にするためタイムゾーンを設定
Time.zone = "Tokyo"

# 更新日にコミット日を使用する
activate :vcs_time