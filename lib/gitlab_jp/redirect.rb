require 'yaml'

module GitlabJp
  class Redirect
    DEFNINTIONS_FILE_PATH = 'data/redirects.yml'

    attr_reader :sources, :target

    def self.each_for_middleman(path = DEFNINTIONS_FILE_PATH)
      data = YAML.load_file(path)

      raise ArgumentError, 'invalid or empty YAML file' unless data.is_a?(Array)

      data.map do |entry|
        sources = Array(entry['sources'])
        target = entry['target']

        sources.map do |source|
          yield(to_middleman_source_path(source), target) if block_given?
        end
      end
    end

    def self.to_middleman_source_path(path)
      path = path.sub(%r|^/|, '')
      if %r(/$) =~ path
        "#{path}index.html"
      else
        path
      end
    end
  end
end
